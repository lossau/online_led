
#include <ESP8266WiFi.h>

const char *ssid = "";
const char *password = "";

int ledPin = D1;
int server_port = 8001;
WiFiServer server(server_port);
String led_status = "";

void blink_led() {
  digitalWrite(ledPin, LOW);
  delay(200);
  digitalWrite(ledPin, HIGH);
  delay(200);
}

void setup() {
  Serial.begin(115200);
  delay(10);

  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, LOW);

  // Connect to WiFi network
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");

  // Start the server
  server.begin();
  Serial.println("Server started");

  // Print the IP address
  Serial.print("Use this URL to connect: ");
  Serial.print("http://");
  Serial.print(WiFi.localIP());
  Serial.print(":");
  Serial.print(server_port);
  Serial.println("/");
}

void loop() {

  if (led_status == "pisca") {
    blink_led();
  }

  // Check if a client has connected
  WiFiClient client = server.available();
  if (!client) {
    return;
  }

  // Wait until the client sends some data
  Serial.println("new client");
  while (!client.available()) {
    delay(1);
  }

  // Read the first line of the request
  String request = client.readStringUntil('\r');
  Serial.println(request);
  client.flush();

  // Match the request
  // Set ledPin according to the request
  if (request.indexOf("/led=liga") != -1) {
    digitalWrite(ledPin, HIGH);
    led_status = "liga";
  }
  if (request.indexOf("/led=desliga") != -1) {
    digitalWrite(ledPin, LOW);
    led_status = "desliga";
  }
  if (request.indexOf("/led=pisca") != -1) {
    led_status = "pisca";
  }

  // Return the response
  client.println("HTTP/1.1 200 OK");
  client.println("Content-Type: text/html");
  client.println(""); //  do not forget this one
  client.println("<!DOCTYPE HTML>");
  client.println("<html>");

  client.print("Status do LED: ");

  client.print(led_status);
  client.println("<br><br>");
  client.println("<a href=\"/led=liga\"\"><button style='font-size:30px;'>Liga "
                 "</button></a>");
  client.println("<a href=\"/led=desliga\"\"><button "
                 "style='font-size:30px;'>Desliga </button></a>");
  client.println("<a href=\"/led=pisca\"\"><button "
                 "style='font-size:30px;'>Pisca </button></a><br />");
  client.println("</html>");

  delay(1);
  Serial.println("Client disonnected");
  Serial.println("");
}

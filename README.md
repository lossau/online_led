# Online LED

Proof of concept of controlling a NodeMCU board over WiFi. This app provides a webserver accepting requests with parameters to enable, disable or blink and LED.

## Components
- NodeMCU 1.0 ESP 12-E